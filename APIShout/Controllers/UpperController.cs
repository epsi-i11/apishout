using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LibSHOUTStr;

namespace APIShout.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class UpperController : ControllerBase
    {
        
        private readonly ILogger<UpperController> _logger;

        public UpperController(ILogger<UpperController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get(string str)
        {
            return str.ToUpper();
        }
    }
}

